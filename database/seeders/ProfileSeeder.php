<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            [
                'id'=>1,
                'name'=>'Administrador',
                'created_at'=>'2023-02-10 11:40:33',
                'updated_at'=>'2023-02-10 11:41:05'
            ],
            [
                'id'=>2,
                'name'=>'Usuario',
                'created_at'=>'2023-02-10 11:40:33',
                'updated_at'=>'2023-02-10 11:41:05'
            ]
        ]);
    }
}
