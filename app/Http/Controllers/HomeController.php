<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveUserRequest;
use App\Models\Disc;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $datos = Disc::all();
        return view('home', compact('datos'));
    }

    public function users(){
        $datos = User::all();
        return view('users', compact('datos'));
    }
    public function usersShow($id){
        $profiles = Profile::all();
        if($id==='new'):
            $datos = [];
        else:
            $datos = User::find($id);
        endif;
        return view('usersForm', compact('datos','profiles'));
    }
    public function usersSave(Request $request, $id){
        if($id != 0):
            $usuario = User::find($id);
            $usuario->name = $request->name;
            $usuario->email = $request->email;
            $usuario->phone = $request->phone;
            $usuario->profile_id = $request->profile_id;
            if(!empty($request->img_photo)):
                $file = $request->img_photo;
                $fileName = $file->getClientOriginalName();
                $file->move(public_path('images/perfil'), $fileName);
                $path = '/images/perfil/'.$fileName;
                $usuario->photo = $path;
            endif;
            $usuario->save();
        else:
            if(!empty($request->img_photo)):
                $file = $request->img_photo;
                $fileName = $file->getClientOriginalName();
                $file->move(public_path('images/perfil'), $fileName);
                $path = '/images/perfil/'.$fileName;
                $request->merge(['photo' => $path]);
            endif;
            User::create([
                "name" => $request->name,
                "email" => $request->email,
                "phone" => $request->phone,
                "photo" => $request->photo,
                "password" => Hash::make($request->password),
                "profile_id" => $request->profile_id
            ]);
        endif;
        return redirect()->route('users');
    }
    public function profiles(){
        $usuario = Auth::user();
        return view('profiles', compact('usuario'));
    }

    public function usersDelete($id){
        $user = User::find($id);
        $ruta_imagen = public_path($user->photo);
        if($user->photo AND file_exists($ruta_imagen)):
            unlink($ruta_imagen);
        endif;
        $user->delete();
        return $user;
    }

    public function getDataUser(){
        $userAuth = Auth::user();
        $profile = Profile::find($userAuth->profile_id);
        return $profile;
    }

    public function prefers(){
        return view('prefers');
    }

    public function savePrefers(Request $request){
        $userAuth = Auth::user();
        $user = User::find($userAuth->id);
        $user->musical_prefer = $request->musical_prefer;
        $user->save();
        return $user;
    }

}
