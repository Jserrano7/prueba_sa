<?php

namespace App\Http\Controllers;

use App\Models\Disc;
use App\Http\Requests\StoreDiscRequest;
use Illuminate\Http\Request;

class DiscController extends Controller
{
    public function index(){
        $datos = Disc::all();
        return view('discs', compact('datos'));
    }

    public function discFilter(Request $request){
        return Disc::where('name', 'LIKE', '%'.$request->name.'%')
            ->where('album', 'LIKE', '%'.$request->album.'%')
            ->where('artist', 'LIKE', '%'.$request->artist.'%')
            ->get();
    }

    public function store(StoreDiscRequest $request, $id){
        $photo = null;
        if(!empty($request->photo)):
            $file = $request->file('photo');
            $fileName = time() .'_' .$file->getClientOriginalName();
            $file->move(public_path('images/discs'), $fileName);
            $path = '/images/discs/'.$fileName;
            $photo = $path;
        endif;
        if($request->id != 0):
            $disc = Disc::find($id);
            if($photo):
                $ruta_imagen = public_path($disc->photo);
                if(file_exists($ruta_imagen)):
                    unlink($ruta_imagen);
                endif;
            endif;
            $disc->name = $request->name;
            $disc->album = $request->album;
            $disc->artist = $request->artist;
            $disc->year = $request->year;
            $disc->gender = $request->gender;
            $disc->photo = $photo ? $photo : $disc->photo;
            $disc->save();
        else:
            Disc::create([
                "name" => $request->name,
                "album" => $request->album,
                "artist" => $request->artist,
                "year" => $request->year,
                "gender" => $request->gender,
                "photo" => $photo
            ]);
        endif;
        return redirect()->route('disc');
    }

    public function show($id){
        if($id === 'new'):
            $datos = null;
        else:
            $datos = Disc::find($id);
        endif;
        return view('discForm', compact('datos'));
    }

    public function destroy($id){
        $disc = Disc::find($id);
        $ruta_imagen = public_path($disc->photo);
        if(file_exists($ruta_imagen)):
            unlink($ruta_imagen);
        endif;
        $disc->delete();
        return $disc;
    }
}
