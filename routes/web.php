<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Auth::routes(['verify' =>true]);

Route::middleware(['auth', 'verified'])->get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth', 'verified'])->group(function(){
    Route::get("/users/form/{id}", [App\Http\Controllers\HomeController::class, 'usersShow'])->name('usersForm');
    Route::post("/users/form/{id}", [App\Http\Controllers\HomeController::class, 'usersSave'])->name('usersSave');
    Route::get("/users", [App\Http\Controllers\HomeController::class, 'users'])->name('users');
    Route::delete("/users/{id}", [App\Http\Controllers\HomeController::class, 'usersDelete'])->name('usersDelete');
    Route::get("/profile", [App\Http\Controllers\HomeController::class, 'profiles'])->name('profiles');
    Route::get("/getDataUser", [App\Http\Controllers\HomeController::class, 'getDataUser']);
    Route::get("/preferencias", [App\Http\Controllers\HomeController::class, 'prefers'])->name('prefers');
    Route::post("/preferencias", [App\Http\Controllers\HomeController::class, 'savePrefers'])->name('savePrefers');

    Route::get("/disc/form/{id}", [App\Http\Controllers\DiscController::class, 'show'])->name('discForm');
    Route::post("/disc/form/{id}", [App\Http\Controllers\DiscController::class, 'store'])->name('discSave');
    Route::get("/disc", [App\Http\Controllers\DiscController::class, 'index'])->name('disc');
    Route::get("/discFilter", [App\Http\Controllers\DiscController::class, 'discFilter'])->name('discFilter');
    Route::delete("/disc/{id}", [App\Http\Controllers\DiscController::class, 'destroy'])->name('disc.delete');
});