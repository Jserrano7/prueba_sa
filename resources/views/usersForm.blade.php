@extends('layouts.app')

@section('content')
  {{-- <users-form
  @if (isset($datos))
    :datos="{{ $datos }}"
  @endif
  ></users-form> --}}
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ isset($datos->id) ? 'Actualizar' : 'Agregar usuario' }}</div>

                <div class="card-body">
                    <form method="POST" action="{{  route('usersSave', isset($datos->id) ? $datos->id : 0) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label for="img_photo" class="col-md-4 col-form-label text-md-end">Foto de perfil</label>

                            <div class="col-md-6">
                                <input id="img_photo" type="file" class="form-control @error('img_photo') is-invalid @enderror" name="img_photo" >
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Nombre completo</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ isset($datos->name) ? $datos->name : old("name") }}" required autocomplete="off" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">Correo</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ isset($datos->email) ? $datos->email : old("email") }}" required autocomplete="off">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="phone" class="col-md-4 col-form-label text-md-end">Telefono</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ isset($datos->phone) ? $datos->phone : old("phone") }}" required autocomplete="off">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="profile_id" class="col-md-4 col-form-label text-md-end">Perfil</label>

                            <div class="col-md-6">
                                <select id="profile_id" class="form-control @error('profile_id') is-invalid @enderror" name="profile_id" required>
                                    <option value="">Selecione</option>
                                    @foreach ($profiles as $item)
                                        <option value="{{ $item->id }}" {{ (isset($datos->profile_id) AND $datos->profile_id === $item->id) ? 'selected' : '' }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>

                                @error('profile_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    @if (!isset($datos->id))
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                    @endif
                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                  Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
