@extends('layouts.app')

@section('content')
  <profiles
  @if (isset($usuario))
    :datos="{{ $usuario }}"
  @endif
  ></profiles>
@endsection
