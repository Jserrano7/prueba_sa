@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Principal') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <discs-list
                    @if (isset($datos))
                        :datos="{{ $datos }}"
                    @endif
                    ></discs-list>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
