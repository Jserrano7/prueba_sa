@extends('layouts.app')

@section('content')
  <discs-form
  @if (isset($datos))
    :datos="{{ $datos }}"
  @endif
  ></discs-form>
@endsection
