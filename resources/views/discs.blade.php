@extends('layouts.app')

@section('content')
  <discs-list
  @if (isset($datos))
    :datos="{{ $datos }}"
  @endif
  ></discs-list>
@endsection
