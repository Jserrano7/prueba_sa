@extends('layouts.app')

@section('content')
  <users-list
  @if (isset($datos))
    :datos="{{ $datos }}"
  @endif
  ></users-list>
@endsection
